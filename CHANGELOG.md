# GitLab License management changelog

## v3.28.9

- Set `asdf` configuration to use Go `1.16.2` by default. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/36)
- Update `curl` links to `get-pip.py`. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/36)

## v3.28.8

- Bump LicenseFinder to 6.12.0 (!37)

## v3.28.7

- Fix bug causing `Go` license scanning to fail if no `go.sum` file exists
  (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/35)

## v3.28.6

- Fix .NET Core not being downloaded after its location has changed
  (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/33)

## v3.28.5

- Bump LicenseFinder to 6.10.0 (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/22)

## v3.28.4

- Set `asdf` configuration to use Go `1.15.5` by default. (!18)
- Fix test by rolling expected version of `cli` forward to `6.14.9`. (!18)

## v3.28.3

- Stream `npm ci` output to log. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/16)

## v3.28.2

- Detect maven wrapper in nested directories. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/15)
- Detect gradle wrapper in nested directories. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/15)

## v3.28.1

- Set `golang` version to `1.15.2` in `.tool-versions` file. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/6)
- Set `ruby` version to `2.7.2` in `.tool-versions` file and other places. (https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/6)

## v3.28.0

- Move project from https://gitlab.com/gitlab-org/security-products/license-management to https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/ https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/3

## v3.27.0

- Parse SPDX License expressions. https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/228

## v3.26.1

- Switch to working directory that contains the `go.mod` file. https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/222

## v3.26.0

- Add Ruby version 2.7.2 to Docker image. https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/226

## v3.25.8

- Remove bash functions that are not in use https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/224

## v3.25.7

- Fix path to Java 11 keystore https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/221

## v3.25.6

- Install `build-essential` for development https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/219
- Export `NODE_EXTRA_CA_CERTS` for self signed certificates https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/219

## v3.25.5

- Remove packages with known vulnerabilities https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/218

## v3.25.4

- Fix patch number of `GOLANG_VERSION` being ignored, making the scan fail if there's no match for MAJOR.MINOR (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/215)

## v3.25.3

- Ensure `apt-get` db is valid for `before_script` blocks. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/215)

## v3.25.2

- Roll out `golang` version `1.15.1` to more of the project. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/216)

## v3.25.1

- Set `golang` version to `1.15.1` in `.tool-versions` file. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/212)

## v3.25.0

- Install tools from Debian package. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/188)

## v3.24.0

- Removes `BSD-4-Clause` from a list of normalized licenses. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/196)

## v3.23.0

- Upgrade [LicenseFinder](https://github.com/pivotal/LicenseFinder/) to `6.8.1` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/209)
- Update SPDX catalogue to  version `3.10` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/209)

## v3.22.2

- Update default PHP to `7.4.8` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/208)

## v3.22.1

- Fix bug with loading `.bashrc`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/206)
- Detect vendor directory for composer projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/207)

## v3.22.0

- Unpack mono from custom built deb package (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/204)

## v3.21.1

- Unpack Rust Debian package before asdf attempts to compile and install it. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/205)

## v3.21.0

- Provide limited network connectivity for [cargo](https://doc.rust-lang.org/cargo/) projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/201)
- Exclude development dependencies from [cargo](https://doc.rust-lang.org/cargo/) project scans (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/201)

## v3.20.1

- Ensure that error messages are converted to strings before writing to the log. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/203)
- Do not reconfigure bundler from the scanners ruby process. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/203)

## v3.20.0

- Fallback to parsing the `composer.lock` file when it is present (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/200)

## v3.19.5

- Fix failing dotnet tests. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/199)

## v3.19.4

- Exclude development dependencies from [composer](https://getcomposer.org) project scans (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/198)

## v3.19.3

- Set `golang` version to `1.14.6` in `.tool-versions` file. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/197)

## v3.19.2

- Combine default/custom x509 certificates in TLS validation. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/194)

## v3.19.1

- Choose a version of the `org.codehaus.mojo:license-maven-plugin:aggregate-download-licenses` that is compatible with the version of Maven used by the project. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/195)
- Print error messages to the console when a scan fails. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/195)

## v3.19.0

- Include the latest LTS of the .NET SDK in the Docker image. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/191)
- Include the latest LTS of Mono in the Docker image. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/193)

## v3.18.1

- Fix failing composer tests.

## v3.18.0

- Improve license detection for NuGet packages (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)
- Detect `.vbproj` files (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)
- Detect `.fsproj` files (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)
- Detect `.sln` files (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)
- Parse SPDX license expressions from `.nuspec` files (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)
- Install custom `ADDITIONAL_CA_CERT_BUNDLE` for `dotnet` CLI projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/189)

## v3.17.2

- Return empty list of packages if bundler scanner fails. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/181)

## v3.17.1

- Reset bundler config from within sub directories in project (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/188)

## v3.17.0

- Isolate the embedded LicenseFinder Ruby from the target project's Ruby (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/181)

## v3.16.0

- Install `dotnet` and `mono` at scan time to decrease size of Docker image. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/185)

## v3.15.0

- Detect `gems.rb` and `gems.locked` in `Bundler` projects. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/186)

## v3.14.0

- Export `BUNDLE_SSL_CA_CERT` when a `ADDITIONAL_CA_CERT_BUNDLE` is provided. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/177)

## v3.13.0

- Upgrade to [`license_finder`](https://github.com/pivotal/LicenseFinder/) [`6.6.0`](https://github.com/pivotal/LicenseFinder/releases/tag/v6.6.0). (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/162) (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/166)

## v3.12.3

- Update default Java versions to match [new naming convention](https://github.com/halcyon/asdf-java/pull/87 (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/176)

## v3.12.2

- Publish latest major version to registry.gitlab.com/gitlab-org/security-products/analyzers/license-finder:3 (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/175)
- Print version of license in log output. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/175)

## v3.12.1

- Detect variations of the "Eclipse Public License" (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/174)

## v3.12.0

- Update default name of the generated report. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/167)

## v3.11.2

- Ensure `LM_*_VERSION` variables take precedence over `ASDF_*_VERSION` variables. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/163)

## v3.11.1

- Fix dependency scanning in golang projects. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/160)
- Parse `go.sum` files to support offline environments (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/161)

## v3.11.0

- Add support for providing custom [Conan](https://conan.io/) configuration. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/159)

## v3.10.0

- Add initial support for the [Conan](https://conan.io/) package manger. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/156)
- Add preview of report version `2.1`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/156)

## v3.9.2

- Pass `bower_ca` to bower install step. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/151)

## v3.9.1

- Add `--allow-root` option when install bower packages. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/150)
- Include nested dependencies in scan report for bower projects. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/150)
- Pass `NPM_CONFIG_CAFILE` to bower install step. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/150)

## v3.9.0

- Update go list command to be compatible with 1.14 (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/143)
- Forward the NPM [cafile](https://docs.npmjs.com/using-npm/config#cafile) option to `yarn`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/148)

## v3.8.1

- Exclude `devDependencies` from `yarn` scan report. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/147)
- Remove `spandx` dependency and bring back Ruby 2.4+ support. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/147)

## v3.8.0

- Add support for NPM [cafile](https://docs.npmjs.com/using-npm/config#cafile) option. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/145)
- Specify path to Java keystore file when listing contents. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/45)

## v3.7.6

- Exclude `devDependencies` from scan report. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/141)
- Include latest NodeJS 12.16.3, 10.20.1 LTS. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/141)
- Update version of PHP to 7.4.5. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/141)
- Update Python to 2.7.18. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/141)
- Update Ruby to 2.6.6. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/141)

## v3.7.5

- Install multiple x509 certificates from `ADDITIONAL_CA_CERT_BUNDLE` into system trust store. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/144)
- Install multiple x509 certificates from `ADDITIONAL_CA_CERT_BUNDLE` into java trust store. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/144)

## v3.7.4

- Install Java key store when `ADDITIONAL_CA_CERT_BUNDLE` is provided. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/139)

## v3.7.3

- Add `--local` option to `gem install` step to speed up initial scan time. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/135)

## v3.7.2

- Forward custom `GRADLE_CLI_OPTS` to `gradle downloadLicenses` and skip additional install step. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/121)

## v3.7.1

- Export `PIP_CERT` when invoking `pip` when a custom root certificate is specified. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/133)

## v3.7.0

- Add `ADDITIONAL_CA_CERT_BUNDLE` to list of trusted root certificates. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/126)

## v3.6.0

- Change default log level to `warn`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/132)
- Allow control of the log level via the `LOG_LEVEL` environment variable. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/132)

## v3.5.0

- Improve license detection in go modules projects. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/129)
- Update gradle to version `6.3`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/129)
- Update nodejs to version `10.19.0`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/129)
- Update php to version `7.4.4`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/129)
- Update python to version `3.8.2`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/129)

## v3.4.0

- Scan pipenv projects with [pip-licenses](https://pypi.org/project/pip-licenses/). (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/130)
- Read pipenv spec data from the sources listed in `Pipfile.lock`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/130)

## v3.3.1

- Fix bug with forwarding `LICENSE_FINDER_CLI_OPTS` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/131)

## v3.3.0

- Scan Python projects with [pip-licenses](https://pypi.org/project/pip-licenses/). (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/128)

## v3.2.0

- Install packages from `PIP_INDEX_URL`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/125)

## v3.1.4

- Print `license-maven-plugin` logs to console. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/127)

## v3.1.3

- Install `license-maven-plugin` into local repository. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/124)

## v3.1.2

- Use `license-maven-plugin:aggreate-download-licenses` for multi-module projects. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/123)

## v3.1.1

- Fix invocation of `SETUP_CMD`. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/122)

## v3.1.0

- Forward custom `MAVEN_CLI_OPTS` to `LicenseFinder` so that it can use it in the license scan task. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/120)

## v3.0.0

- Use asdf version manager to install custom tools (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/98)

## v2.8.0

- Install `php-gd` package to support default Drupal configurations. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/114)
- Allow to set JAVA_VERSION when you use SETUP_CMD. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/119)

## v2.7.0

- Install project specific versions of gradle at scan time. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/118)

## v2.6.0

- Upgrade to license finder 6.0.0 docker image (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/115)

## v2.5.2

- Exclude development/test dependencies by default (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/117)

## v2.5.1

- Install bundler `1.x` and `2.x` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/116)

## v2.5.0

- Upgrade [LicenseFinder](https://github.com/pivotal/LicenseFinder/releases/tag/v6.0.0) to version `6.0.0` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/112)

## v2.4.3

- Add support for `gradlew` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/109)
- Install [license-gradle-plugin](https://github.com/hierynomus/license-gradle-plugin) in gradle init script. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/109)

## v2.4.2

- Fix word splitting in default gradle options (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/110)

## v2.4.1

- Include a default NuGet configuration file (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/105)

## v2.4.0

- Add support for `Pipfile.lock` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/103)

## v2.3.1

- Run gradle without tests by default. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/102)

## v2.3.0

- Install Python 3.8.1 as the default python (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/101)

## v2.2.3

- Add a mapping for `BSD-like` software licenses. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/97)

## v2.2.2

- Install the latest version of pip for both Python 2 and 3 at build time (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/99)

## v2.2.1

- Use `--prepare-no-fail` option to try to scan as much as possible. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/92)

## v2.2.0

- Update LicenseFinder to 5.11.1 [Diff](https://github.com/pivotal/LicenseFinder/compare/v5.9.2...v5.11.1)

## v2.1.1

- Sort license identifiers associated with each dependency

## v2.1.0

- Bump LicenseFinder to 5.9.2
- Add support for PHP language

## v2.0.2

- Fix mismatch between dependency versions listed in `package-lock.json` and reported versions.

## v2.0.1

- Sort dependencies by name then by version

## v2.0.0

- Update the default report version to v2.0 (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/66)

## v1.8.3

- Update java 11 from 11.0.2 to 11.0.5

## v1.8.2

- Ignore node version for installing npm dependencies(https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/79)

## v1.8.1

- Add mapping for `Apache License v2.0` to `Apache-2.0` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/78)

## v1.8.0

- Add ability to configure the `license_finder` execution via `LICENSE_FINDER_CLI_OPTS` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/77)

## v1.7.4

- Install [.NET Core 2.2, 3.0](https://github.com/pivotal/LicenseFinder/pull/632) so that we can install packages for .NET Core 2.2, 3.0 projects.
- Parse SPDX identifier from license URL sourced from `licenses.nuget.org` and `opensource.org`.
- Use lower case license name as identifier for unknown licenses to remove ambiguity.

## v1.7.3

- Update SPDX.org catalogue from version `3.6` to `3.7`.

## v1.7.2

- Remove `LM_V1_CANONICALIZE` feature flag
- Remove `FEATURE_RUBY_REPORT` feature flag
- Remove `html2json.js` script

## v1.7.1

- Add mappings for legacy license names
- Use the license url instead of details url

## v1.7.0

- Convert HTML to JSON transformation to generating a JSON report directly.
- Introduce version 1.1 of the gl-license-management-report.json
- Introduce version 2 of the gl-license-management-report.json.
- Add support for multiple licenses.
- Normalize license names to produce consistent results.

## v1.6.1

- Fix `The engine "node" is incompatible with this module.` error (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/61)

## v1.6.0

- Make Python 3.5 the default. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/56)

## v1.5.0

- Reverts 1.4.0

## v1.4.0

- Bump LicenseFinder to 5.9.2
- Add support for PhP language

## v1.3.0

- Add `LM_PYTHON_VERSION` variable, to be set to `3` to switch to Python 3.5, pip 19.1.1. (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/36)

## v1.2.6

- Fix: better support of go projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/31)

## v1.2.5

- Feature: support Java 11 via an ENV variable (@haynes https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/26)

## v1.2.4

- Fix: support multiple MAVEN_CLI_OPTS options (@haynes https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/27)

## v1.2.3

- Add ability to configure the `mvn install` execution for Maven projects via `MAVEN_CLI_OPTS` (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/24)
- Skip `"test"` phase by default when running `mvn install` for Maven projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/24)

## v1.2.2

- Bump LicenseFinder to 5.6.2

## v1.2.1

- Better support for js npm projects (https://gitlab.com/gitlab-org/security-products/license-management/-/merge_requests/14)

## v1.2.0

- Bump LicenseFinder to 5.5.2

## v1.1.0

- Allow `SETUP_CMD` to skip auto-detection of build tool

## v1.0.0

- Initial release
