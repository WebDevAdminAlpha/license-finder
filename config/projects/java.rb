# frozen_string_literal: true

java_version = ENV.fetch('JAVA_VERSION', '11')

name "java-#{java_version}"
maintainer "GitLab B.V."
homepage "https://adoptopenjdk.net/"

install_dir "/opt/asdf/installs/java/#{java_version}"
package_scripts_path Pathname.pwd.join("config/scripts/java")

build_version java_version
build_iteration 1

override 'asdf_java', version: java_version
dependency "asdf_java"

package :deb do
  compression_level 9
  compression_type :xz
end
