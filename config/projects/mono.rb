# frozen_string_literal: true

mono_version = ENV.fetch('MONO_VERSION', '6.8.0.123')

name "mono-#{mono_version}"
maintainer "GitLab B.V."
homepage "https://www.mono-project.com/"

install_dir "/opt/asdf/installs/mono/#{mono_version}"
package_scripts_path Pathname.pwd.join("config/scripts/mono")

build_version mono_version
build_iteration 1

override 'asdf_mono', version: mono_version
dependency "asdf_mono"

package :deb do
  compression_level 9
  compression_type :xz
end
