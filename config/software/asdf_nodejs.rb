# frozen_string_literal: true

name "asdf_nodejs"
default_version "12.18.2"

source url: "https://nodejs.org/dist/v#{version}/node-v#{version}-linux-x64.tar.gz"
relative_path "node-v#{version}-linux-x64"

version "12.18.2" do
  source sha256: "2d316e55994086e41761b0c657e0027e9d16d7160d3f8854cc9dc7615b99a526"
end
version "10.21.0" do
  source sha256: "d0bac246001eed9268ba9cadbfc6cfd8b6eb0728ad000a0f9fa7ce29e66c2be4"
end

build do
  mkdir install_dir
  sync "#{project_dir}/", install_dir
  touch "#{install_dir}/.npm/.keep"
end

build do
  command "#{install_dir}/bin/node #{install_dir}/bin/npm install -g bower bower-npm-resolver npm@6 npm-install-peers yarn"
end

build do
  delete "#{install_dir}/share"
end
