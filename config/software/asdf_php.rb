# frozen_string_literal: true

name "asdf_php"
default_version "7.4.8"
license_file "LICENSE"

dependency "autoconf"
dependency "libtool"
dependency "libxml2"
dependency "openssl"
dependency "zlib"
dependency "libsqlite3"

source url: "https://www.php.net/distributions/php-#{version}.tar.gz"
relative_path "php-#{version}"

version("7.4.8") { source sha256: "649f6bcdb60dc38d5edd7f3a7b2905d15d88c1d13e40307e8972ede347cea6ba" }

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--prefix=#{install_dir}",
    "--with-openssl=#{install_dir}/embedded"
  ]
  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
end

build do
  command "curl https://getcomposer.org/installer -o #{project_dir}/composer-setup.php"
  command "#{install_dir}/bin/php #{project_dir}/composer-setup.php --install-dir=#{install_dir}/bin --filename=composer"
end

build do
  delete "#{install_dir}/embedded/bin/sqlite3"
end
