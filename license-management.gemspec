# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'license/management/version'

SCANNER_VERSION = '6.12.0'

Gem::Specification.new do |spec|
  spec.name          = 'license-management'
  spec.version       = License::Management::VERSION
  spec.authors       = ['Fabien Catteau', 'Olivier Gonzalez', 'mo khan']
  spec.email         = ['fcatteau@gitlab.com', 'ogonzalez@gitlab.com', 'mkhan@gitlab.com']

  spec.summary       = 'License Management job for GitLab CI.'
  spec.description   = 'License Management job for GitLab CI. https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html'
  spec.homepage      = 'https://gitlab.com/gitlab-org/security-products/analyzers/license-finder'
  spec.license       = 'Nonstandard'

  spec.metadata['allowed_push_host'] = 'https://example.com'
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/security-products/analyzers/license-finder'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/blob/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob('exe/*') + Dir.glob('lib/**/**/*.{rb,yml}') + Dir.glob('*.{md,yml,json}')
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'license_finder', SCANNER_VERSION
  spec.add_dependency 'spandx', '~> 0.13'
  spec.add_development_dependency 'byebug', '~> 11.1'
  spec.add_development_dependency 'gitlab-styles', '~> 3.1'
  spec.add_development_dependency 'json-schema', '~> 2.8'
  spec.add_development_dependency 'omnibus', '~> 7.0'
  spec.add_development_dependency 'rspec', '~> 3.9'
  spec.add_development_dependency 'rspec-benchmark', '~> 0.6'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.4'
  spec.add_development_dependency 'simplecov', '~> 0.18'
  spec.add_development_dependency 'simplecov-cobertura', '~> 1.3'
end
