# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "conan" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  include_examples "each report version", "c", "conan", "754965b0"

  context "when scanning a C++ project" do
    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/poco/md5' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify { expect { subject }.to perform_under(60).sec.warmup(0).times }

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(%w[bzip2 expat openssl pcre poco sqlite3 zlib])
      expect(subject.licenses_for('openssl')).to match_array(['OpenSSL'])
      expect(subject.licenses_for('poco')).to match_array(['BSL-1.0'])
    end
  end

  context "when scanning a folly project" do
    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/folly/basic' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify do
      expect(subject).to match_schema
      expect(subject.licenses_for('boost')).to match_array(['BSL-1.0'])
      expect(subject.licenses_for('bzip2')).to match_array(['bzip2-1.0.8'])
      expect(subject.licenses_for('double-conversion')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('folly')).to match_array(['Apache-2.0'])
      expect(subject.licenses_for('gflags')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('glog')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('libdwarf')).to match_array(['LGPL-2.1'])
      expect(subject.licenses_for('libelf')).to match_array(['LGPL-2.0'])
      expect(subject.licenses_for('libevent')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('libiberty')).to match_array(['LGPL-2.1'])
      expect(subject.licenses_for('libsodium')).to match_array(['ISC'])
      expect(subject.licenses_for('libunwind')).to match_array(['MIT'])
      expect(subject.licenses_for('lz4')).to match_array(['BSD-2-Clause', 'BSD-3-Clause'])
      expect(subject.licenses_for('openssl')).to match_array(['OpenSSL'])
      expect(subject.licenses_for('snappy')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('zlib')).to match_array(['Zlib'])
      expect(subject.licenses_for('zstd')).to match_array(['BSD-3-Clause'])
    end
  end

  context "when scanning a project with cmake" do
    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/protobuf/serialization' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(%w[protobuf protoc_installer])
      expect(subject.licenses_for('protobuf')).to match_array(['BSD-3-Clause'])
      expect(subject.licenses_for('protoc_installer')).to match_array(['BSD-3-Clause'])
    end
  end

  context "when pulling packages from a custom conan remote" do
    let(:package_name) { "#{project_namespace.tr('/', '+')}+#{project_name}/stable" }
    let(:project_namespace) { ENV.fetch('CI_PROJECT_NAMESPACE', 'gitlab-org/security-products/analyzers') }
    let(:project_name) { ENV.fetch('CI_PROJECT_NAME', 'license-finder') }
    let(:api_url) { ENV.fetch('CI_API_V4_URL', 'https://gitlab.com/api/v4') }

    before do
      runner.mount(dir: fixture_file('c/conan/example-project'))
      runner.add_file('conanfile.txt', fixture_file_content('c/conan/example-project/conanfile.txt.erb', package_name: package_name))
      runner.add_file('.conan/remotes.json') do
        JSON.pretty_generate({
          remotes: [
            {
              name: 'gitlab',
              url: "#{api_url}/packages/conan",
              verify_ssl: true
            }
          ]
        })
      end
    end

    # https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/merge_requests/3/diffs#note_424952493
    pending do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(['example'])
      expect(subject.licenses_for('example')).to match_array(['MIT'])
    end
  end
end
