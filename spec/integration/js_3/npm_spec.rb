# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "npm" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  context 'when scanning a project with a single dependency and a `package-lock.json` file' do
    before do
      runner.mount(dir: fixture_file('js/single-declared-dependency'))
    end

    specify { expect(subject).to match_schema }

    specify do
      [
        ["2md", "0.0.4", ["Apache-2.0"]],
        ["abab", "2.0.3", ["BSD-3-Clause"]],
        ["acorn", "6.4.1", ["MIT"]],
        ["acorn", "7.1.1", ["MIT"]],
        ["acorn-globals", "4.3.4", ["MIT"]],
        ["acorn-walk", "6.2.0", ["MIT"]],
        ["ajv", "6.12.2", ["MIT"]],
        ["ansi-regex", "4.1.0", ["MIT"]],
        ["ansi-styles", "3.2.1", ["MIT"]],
        ["array-equal", "1.0.0", ["MIT"]],
        ["asn1", "0.2.4", ["MIT"]],
        ["assert-plus", "1.0.0", ["MIT"]],
        ["asynckit", "0.4.0", ["MIT"]],
        ["aws-sign2", "0.7.0", ["Apache-2.0"]],
        ["aws4", "1.9.1", ["MIT"]],
        ["bcrypt-pbkdf", "1.0.2", ["BSD-3-Clause"]],
        ["browser-process-hrtime", "1.0.0", ["BSD-2-Clause"]],
        ["camelcase", "5.3.1", ["MIT"]],
        ["caseless", "0.12.0", ["Apache-2.0"]],
        ["cliui", "5.0.0", ["ISC"]],
        ["color-convert", "1.9.3", ["MIT"]],
        ["color-name", "1.1.3", ["MIT"]],
        ["combined-stream", "1.0.8", ["MIT"]],
        ["core-util-is", "1.0.2", ["MIT"]],
        ["cssom", "0.3.8", ["MIT"]],
        ["cssom", "0.4.4", ["MIT"]],
        ["cssstyle", "2.2.0", ["MIT"]],
        ["dashdash", "1.14.1", ["MIT"]],
        ["data-urls", "1.1.0", ["MIT"]],
        ["decamelize", "1.2.0", ["MIT"]],
        ["deep-is", "0.1.3", ["MIT"]],
        ["delayed-stream", "1.0.0", ["MIT"]],
        ["domexception", "1.0.1", ["MIT"]],
        ["ecc-jsbn", "0.1.2", ["MIT"]],
        ["emoji-regex", "7.0.3", ["MIT"]],
        ["escodegen", "1.14.1", ["BSD-2-Clause"]],
        ["esprima", "4.0.1", ["BSD-2-Clause"]],
        ["estraverse", "4.3.0", ["BSD-2-Clause"]],
        ["esutils", "2.0.3", ["BSD-2-Clause"]],
        ["extend", "3.0.2", ["MIT"]],
        ["extsprintf", "1.3.0", ["MIT"]],
        ["fast-deep-equal", "3.1.1", ["MIT"]],
        ["fast-json-stable-stringify", "2.1.0", ["MIT"]],
        ["fast-levenshtein", "2.0.6", ["MIT"]],
        ["find-up", "3.0.0", ["MIT"]],
        ["forever-agent", "0.6.1", ["Apache-2.0"]],
        ["form-data", "2.3.3", ["MIT"]],
        ["fs-extra", "8.1.0", ["MIT"]],
        ["get-caller-file", "2.0.5", ["ISC"]],
        ["getpass", "0.1.7", ["MIT"]],
        ["graceful-fs", "4.2.3", ["ISC"]],
        ["grapheme-splitter", "1.0.4", ["MIT"]],
        ["har-schema", "2.0.0", ["ISC"]],
        ["har-validator", "5.1.3", ["MIT"]],
        ["html-encoding-sniffer", "1.0.2", ["MIT"]],
        ["http-signature", "1.2.0", ["MIT"]],
        ["iconv-lite", "0.4.24", ["MIT"]],
        ["ip-regex", "2.1.0", ["MIT"]],
        ["is-fullwidth-code-point", "2.0.0", ["MIT"]],
        ["is-typedarray", "1.0.0", ["MIT"]],
        ["isstream", "0.1.2", ["MIT"]],
        ["jsbn", "0.1.1", ["MIT"]],
        ["jsdom", "15.2.1", ["MIT"]],
        ["json-schema", "0.2.3", ["AFL-2.1", "bsd"]],
        ["json-schema-traverse", "0.4.1", ["MIT"]],
        ["json-stringify-safe", "5.0.1", ["ISC"]],
        ["jsonfile", "4.0.0", ["MIT"]],
        ["jsprim", "1.4.1", ["MIT"]],
        ["levn", "0.3.0", ["MIT"]],
        ["locate-path", "3.0.0", ["MIT"]],
        ["lodash", "4.17.15", ["MIT"]],
        ["lodash.sortby", "4.7.0", ["MIT"]],
        ["mime-db", "1.43.0", ["MIT"]],
        ["mime-types", "2.1.26", ["MIT"]],
        ["nwsapi", "2.2.0", ["MIT"]],
        ["oauth-sign", "0.9.0", ["Apache-2.0"]],
        ["optionator", "0.8.3", ["MIT"]],
        ["p-limit", "2.3.0", ["MIT"]],
        ["p-locate", "3.0.0", ["MIT"]],
        ["p-try", "2.2.0", ["MIT"]],
        ["parse5", "5.1.0", ["MIT"]],
        ["path-exists", "3.0.0", ["MIT"]],
        ["performance-now", "2.1.0", ["MIT"]],
        ["pn", "1.1.0", ["MIT"]],
        ["prelude-ls", "1.1.2", ["MIT"]],
        ["psl", "1.8.0", ["MIT"]],
        ["punycode", "2.1.1", ["MIT"]],
        ["qs", "6.5.2", ["BSD-3-Clause"]],
        ["regenerator-runtime", "0.13.5", ["MIT"]],
        ["request", "2.88.2", ["Apache-2.0"]],
        ["request-promise-core", "1.1.3", ["ISC"]],
        ["request-promise-native", "1.0.8", ["ISC"]],
        ["require-directory", "2.1.1", ["MIT"]],
        ["require-main-filename", "2.0.0", ["ISC"]],
        ["safe-buffer", "5.2.0", ["MIT"]],
        ["safer-buffer", "2.1.2", ["MIT"]],
        ["saxes", "3.1.11", ["ISC"]],
        ["set-blocking", "2.0.0", ["ISC"]],
        ["source-map", "0.6.1", ["BSD-3-Clause"]],
        ["sshpk", "1.16.1", ["MIT"]],
        ["stealthy-require", "1.1.1", ["ISC"]],
        ["string-width", "3.1.0", ["MIT"]],
        ["strip-ansi", "5.2.0", ["MIT"]],
        ["symbol-tree", "3.2.4", ["MIT"]],
        ["tough-cookie", "2.5.0", ["BSD-3-Clause"]],
        ["tough-cookie", "3.0.1", ["BSD-3-Clause"]],
        ["tr46", "1.0.1", ["MIT"]],
        ["tunnel-agent", "0.6.0", ["Apache-2.0"]],
        ["tweetnacl", "0.14.5", ["Unlicense"]],
        ["type-check", "0.3.2", ["MIT"]],
        ["universalify", "0.1.2", ["MIT"]],
        ["uri-js", "4.2.2", ["BSD-2-Clause"]],
        ["uuid", "3.4.0", ["MIT"]],
        ["verror", "1.10.0", ["MIT"]],
        ["w3c-hr-time", "1.0.2", ["MIT"]],
        ["w3c-xmlserializer", "1.1.2", ["MIT"]],
        ["webidl-conversions", "4.0.2", ["BSD-2-Clause"]],
        ["whatwg-encoding", "1.0.5", ["MIT"]],
        ["whatwg-mimetype", "2.3.0", ["MIT"]],
        ["whatwg-url", "7.1.0", ["MIT"]],
        ["which-module", "2.0.0", ["ISC"]],
        ["word-wrap", "1.2.3", ["MIT"]],
        ["wrap-ansi", "5.1.0", ["MIT"]],
        ["ws", "7.2.3", ["MIT"]],
        ["xml-name-validator", "3.0.0", ["Apache-2.0"]],
        ["xmlchars", "2.2.0", ["MIT"]],
        ["y18n", "4.0.0", ["ISC"]],
        ["yargs", "14.2.3", ["MIT"]],
        ["yargs-parser", "15.0.1", ["ISC"]]
      ].each do |item|
        expect(subject.dependency_names).to include(item[0])
        expect(subject.licenses_for(item[0])).to match_array(item[-1])
      end
    end
  end

  context 'when scanning a project without a `package-lock.json` file' do
    before do
      runner.add_file('package.json', fixture_file_content('js/single-declared-dependency/package.json'))
    end

    specify { expect(subject).to match_schema }

    specify do
      [
        ["2md", "0.0.4", ["Apache-2.0"]],
        ["abab", "2.0.3", ["BSD-3-Clause"]],
        ["acorn", "6.4.1", ["MIT"]],
        ["acorn", "7.1.1", ["MIT"]],
        ["acorn-globals", "4.3.4", ["MIT"]],
        ["acorn-walk", "6.2.0", ["MIT"]],
        ["ajv", "6.12.2", ["MIT"]],
        ["ansi-regex", "4.1.0", ["MIT"]],
        ["ansi-styles", "3.2.1", ["MIT"]],
        ["array-equal", "1.0.0", ["MIT"]],
        ["asn1", "0.2.4", ["MIT"]],
        ["assert-plus", "1.0.0", ["MIT"]],
        ["asynckit", "0.4.0", ["MIT"]],
        ["aws-sign2", "0.7.0", ["Apache-2.0"]],
        ["aws4", "1.9.1", ["MIT"]],
        ["bcrypt-pbkdf", "1.0.2", ["BSD-3-Clause"]],
        ["browser-process-hrtime", "1.0.0", ["BSD-2-Clause"]],
        ["camelcase", "5.3.1", ["MIT"]],
        ["caseless", "0.12.0", ["Apache-2.0"]],
        ["cliui", "5.0.0", ["ISC"]],
        ["color-convert", "1.9.3", ["MIT"]],
        ["color-name", "1.1.3", ["MIT"]],
        ["combined-stream", "1.0.8", ["MIT"]],
        ["core-util-is", "1.0.2", ["MIT"]],
        ["cssom", "0.3.8", ["MIT"]],
        ["cssom", "0.4.4", ["MIT"]],
        ["cssstyle", "2.2.0", ["MIT"]],
        ["dashdash", "1.14.1", ["MIT"]],
        ["data-urls", "1.1.0", ["MIT"]],
        ["decamelize", "1.2.0", ["MIT"]],
        ["deep-is", "0.1.3", ["MIT"]],
        ["delayed-stream", "1.0.0", ["MIT"]],
        ["domexception", "1.0.1", ["MIT"]],
        ["ecc-jsbn", "0.1.2", ["MIT"]],
        ["emoji-regex", "7.0.3", ["MIT"]],
        ["escodegen", "1.14.1", ["BSD-2-Clause"]],
        ["esprima", "4.0.1", ["BSD-2-Clause"]],
        ["estraverse", "4.3.0", ["BSD-2-Clause"]],
        ["esutils", "2.0.3", ["BSD-2-Clause"]],
        ["extend", "3.0.2", ["MIT"]],
        ["extsprintf", "1.3.0", ["MIT"]],
        ["fast-deep-equal", "3.1.1", ["MIT"]],
        ["fast-json-stable-stringify", "2.1.0", ["MIT"]],
        ["fast-levenshtein", "2.0.6", ["MIT"]],
        ["find-up", "3.0.0", ["MIT"]],
        ["forever-agent", "0.6.1", ["Apache-2.0"]],
        ["form-data", "2.3.3", ["MIT"]],
        ["fs-extra", "8.1.0", ["MIT"]],
        ["get-caller-file", "2.0.5", ["ISC"]],
        ["getpass", "0.1.7", ["MIT"]],
        ["graceful-fs", "4.2.3", ["ISC"]],
        ["grapheme-splitter", "1.0.4", ["MIT"]],
        ["har-schema", "2.0.0", ["ISC"]],
        ["har-validator", "5.1.3", ["MIT"]],
        ["html-encoding-sniffer", "1.0.2", ["MIT"]],
        ["http-signature", "1.2.0", ["MIT"]],
        ["iconv-lite", "0.4.24", ["MIT"]],
        ["ip-regex", "2.1.0", ["MIT"]],
        ["is-fullwidth-code-point", "2.0.0", ["MIT"]],
        ["is-typedarray", "1.0.0", ["MIT"]],
        ["isstream", "0.1.2", ["MIT"]],
        ["jsbn", "0.1.1", ["MIT"]],
        ["jsdom", "15.2.1", ["MIT"]],
        ["json-schema", "0.2.3", ["AFL-2.1", "bsd"]],
        ["json-schema-traverse", "0.4.1", ["MIT"]],
        ["json-stringify-safe", "5.0.1", ["ISC"]],
        ["jsonfile", "4.0.0", ["MIT"]],
        ["jsprim", "1.4.1", ["MIT"]],
        ["levn", "0.3.0", ["MIT"]],
        ["locate-path", "3.0.0", ["MIT"]],
        ["lodash", "4.17.15", ["MIT"]],
        ["lodash.sortby", "4.7.0", ["MIT"]],
        ["mime-db", "1.43.0", ["MIT"]],
        ["mime-types", "2.1.26", ["MIT"]],
        ["nwsapi", "2.2.0", ["MIT"]],
        ["oauth-sign", "0.9.0", ["Apache-2.0"]],
        ["optionator", "0.8.3", ["MIT"]],
        ["p-limit", "2.3.0", ["MIT"]],
        ["p-locate", "3.0.0", ["MIT"]],
        ["p-try", "2.2.0", ["MIT"]],
        ["parse5", "5.1.0", ["MIT"]],
        ["path-exists", "3.0.0", ["MIT"]],
        ["performance-now", "2.1.0", ["MIT"]],
        ["pn", "1.1.0", ["MIT"]],
        ["prelude-ls", "1.1.2", ["MIT"]],
        ["psl", "1.8.0", ["MIT"]],
        ["punycode", "2.1.1", ["MIT"]],
        ["qs", "6.5.2", ["BSD-3-Clause"]],
        ["regenerator-runtime", "0.13.5", ["MIT"]],
        ["request", "2.88.2", ["Apache-2.0"]],
        ["request-promise-core", "1.1.3", ["ISC"]],
        ["request-promise-native", "1.0.8", ["ISC"]],
        ["require-directory", "2.1.1", ["MIT"]],
        ["require-main-filename", "2.0.0", ["ISC"]],
        ["safe-buffer", "5.2.0", ["MIT"]],
        ["safer-buffer", "2.1.2", ["MIT"]],
        ["saxes", "3.1.11", ["ISC"]],
        ["set-blocking", "2.0.0", ["ISC"]],
        ["source-map", "0.6.1", ["BSD-3-Clause"]],
        ["sshpk", "1.16.1", ["MIT"]],
        ["stealthy-require", "1.1.1", ["ISC"]],
        ["string-width", "3.1.0", ["MIT"]],
        ["strip-ansi", "5.2.0", ["MIT"]],
        ["symbol-tree", "3.2.4", ["MIT"]],
        ["tough-cookie", "2.5.0", ["BSD-3-Clause"]],
        ["tough-cookie", "3.0.1", ["BSD-3-Clause"]],
        ["tr46", "1.0.1", ["MIT"]],
        ["tunnel-agent", "0.6.0", ["Apache-2.0"]],
        ["tweetnacl", "0.14.5", ["Unlicense"]],
        ["type-check", "0.3.2", ["MIT"]],
        ["universalify", "0.1.2", ["MIT"]],
        ["uri-js", "4.2.2", ["BSD-2-Clause"]],
        ["uuid", "3.4.0", ["MIT"]],
        ["verror", "1.10.0", ["MIT"]],
        ["w3c-hr-time", "1.0.2", ["MIT"]],
        ["w3c-xmlserializer", "1.1.2", ["MIT"]],
        ["webidl-conversions", "4.0.2", ["BSD-2-Clause"]],
        ["whatwg-encoding", "1.0.5", ["MIT"]],
        ["whatwg-mimetype", "2.3.0", ["MIT"]],
        ["whatwg-url", "7.1.0", ["MIT"]],
        ["which-module", "2.0.0", ["ISC"]],
        ["word-wrap", "1.2.3", ["MIT"]],
        ["wrap-ansi", "5.1.0", ["MIT"]],
        ["ws", "7.2.3", ["MIT"]],
        ["xml-name-validator", "3.0.0", ["Apache-2.0"]],
        ["xmlchars", "2.2.0", ["MIT"]],
        ["y18n", "4.0.0", ["ISC"]],
        ["yargs", "14.2.3", ["MIT"]],
        ["yargs-parser", "15.0.1", ["ISC"]]
      ].each do |item|
        expect(subject.dependency_names).to include(item[0])
        expect(subject.licenses_for(item[0])).to match_array(item.last)
      end
    end
  end

  context "when scanning a project with dependencies sourced from a custom registry" do
    let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read } }

    let(:private_npm_host) { 'npm.test' }

    before do
      runner.add_file(".npmrc", "registry = https://#{private_npm_host}")
      runner.add_file("package.json") do
        JSON.pretty_generate({
          name: "js-npm",
          version: "1.0.0",
          description: "Test project for js-npm",
          dependencies: { lodash: "4.17.10" },
          devDependencies: {},
          scripts: { test: "echo 'test'" }
        })
      end
      runner.add_file("package-lock.json") do
        JSON.pretty_generate({
          name: "js-npm",
          version: "1.0.0",
          lockfileVersion: 1,
          requires: true,
          dependencies: {
            lodash: {
              version: "4.17.10",
              resolved: "https://#{private_npm_host}/lodash/-/lodash-4.17.10.tgz",
              integrity: "sha512-UejweD1pDoXu+AD825lWwp4ZGtSwgnpZxb3JDViD7StjQz+Nb/6l093lx4OQ0foGWNRoc19mWy7BzL+UAK2iVg=="
            }
          }
        })
      end
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.dependency_names).to match_array(%w[js-npm lodash]) }
    specify { expect(subject.licenses_for('js-npm')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('lodash')).to match_array(['MIT']) }
  end
end
