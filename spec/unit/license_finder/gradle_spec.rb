# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Gradle do
  let(:package_manager) { described_class.new(options) }
  let(:options) { { ignored_groups: [], project_path: project.project_path } }
  let(:project) { ProjectHelper.new }
  let(:project_fixture) { fixture_file('java/gradle/java-8') }

  before do
    project.mount(dir: project_fixture)
  end

  after do
    project.cleanup
  end

  describe "#java_version" do
    subject { package_manager.send(:java_version, env: env) }

    let(:env) { {} }

    context "when the version is specified in a .tool-versions file" do
      let(:project_fixture) { fixture_file('java/maven/tool-versions') }

      specify { expect(subject).to eql('tool-versions') }
    end

    context "when the version is specified in a .java-version file" do
      let(:project_fixture) { fixture_file('java/maven/java-version') }

      specify { expect(subject).to eql('java-version') }
    end

    context "when the version is specified via a ASDF_JAVA_VERSION environment variable" do
      let(:env) { { "ASDF_JAVA_VERSION" => 'adopt-openjdk-11' } }

      specify { expect(subject).to eql('adoptopenjdk-11') }
    end

    %w[8 11].each do |java_version|
      context "when the version is specified via a LM_JAVA_VERSION (#{java_version}) environment variable" do
        let(:env) { { "LM_JAVA_VERSION" => java_version } }

        specify { expect(subject).to eql(java_version) }
      end
    end

    context "when LM_JAVA_VERSION and ASDF_JAVA_VERSION is provided" do
      let(:env) do
        {
          'ASDF_JAVA_VERSION' => 'adopt-openjdk-11',
          'LM_JAVA_VERSION' => '8'
        }
      end

      specify { expect(subject).to eql('8') }
    end

    context 'when a custom Java version is not specified' do
      specify { expect(subject).to eql('8') }
    end
  end
end
