# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Pip do
  let(:package_manager) { described_class.new(options) }
  let(:options) { { ignored_groups: [], project_path: project.project_path } }
  let(:project) { ProjectHelper.new }
  let(:project_fixture) { fixture_file('python/pip/requirements') }

  before do
    project.mount(dir: project_fixture)
  end

  after do
    project.cleanup
  end

  describe "#python_version" do
    subject { package_manager.send(:python_version, env: env) }

    let(:env) { {} }

    context "when the version is specified in a .tool-versions file" do
      before do
        project.add_file(".tool-versions", "python 3.8.1")
      end

      specify { expect(subject).to eql('3.8.5') }
    end

    context "when the version is specified in a .python-version file" do
      before do
        project.add_file(".python-version", "3.8.0")
      end

      specify { expect(subject).to eql('3.8.5') }
    end

    context "when the version is specified via a ASDF_PYTHON_VERSION environment variable" do
      let(:env) { { "ASDF_PYTHON_VERSION" => '3.8.1' } }

      specify { expect(subject).to eql('3.8.5') }
    end

    [['2', '2.7.18'], ['3', '3.8.5']].each do |(major, version)|
      context "when the version is specified via a LM_PYTHON_VERSION (#{major}) environment variable" do
        let(:env) { { "LM_PYTHON_VERSION" => major } }

        specify { expect(subject).to eql(version) }
      end
    end

    context "when LM_PYTHON_VERSION and ASDF_PYTHON_VERSION is provided" do
      let(:env) do
        {
          'ASDF_PYTHON_VERSION' => '2.7.19',
          'LM_PYTHON_VERSION' => '3'
        }
      end

      specify { expect(subject).to eql('3.8.5') }
    end

    context 'when a custom Python version is not specified' do
      specify { expect(subject).to eql('3.8.5') }
    end
  end
end
